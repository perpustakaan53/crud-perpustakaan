package com.perpustakaan.crud.repository;

import com.perpustakaan.crud.model.Perpustakaan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PerpustakaanRepository extends JpaRepository<Perpustakaan, Long> {

}
