package com.perpustakaan.crud.model;

import net.bytebuddy.dynamic.loading.InjectionClassLoader;
import org.springframework.web.bind.annotation.GetMapping;

import javax.persistence.*;

@Entity
@Table(name = "perpustakaan")
public class Perpustakaan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long No;


    @Column(name = "nama_peminjam")
    private String namaPeminjam;

    @Column(name = "nama_buku")
    private String namaBuku;

    @Column(name = "lama_peminjaman")
    private String lamaPeminjaman;

    @Column(name = "status")
    private String status;

    @Column(name = "keterangan")
    private String keterangan;

    public Perpustakaan() {
    }

    @Override
    public String toString() {
        return "Perpustakaan{" +
                "No=" + No +
                ", namaPeminjam='" + namaPeminjam + '\'' +
                ", namaBuku='" + namaBuku + '\'' +
                ", lamaPeminjaman='" + lamaPeminjaman + '\'' +
                ", status='" + status + '\'' +
                ", keterangan='" + keterangan + '\'' +
                '}';
    }

    public Long getNo() {
        return No;
    }

    public void setNo(Long no) {
        No = no;
    }

    public String getNamaPeminjam() {
        return namaPeminjam;
    }

    public void setNamaPeminjam(String namaPeminjam) {
        this.namaPeminjam = namaPeminjam;
    }

    public String getNamaBuku() {
        return namaBuku;
    }

    public void setNamaBuku(String namaBuku) {
        this.namaBuku = namaBuku;
    }

    public String getLamaPeminjaman() {
        return lamaPeminjaman;
    }

    public void setLamaPeminjaman(String lamaPeminjaman) {
        this.lamaPeminjaman = lamaPeminjaman;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
