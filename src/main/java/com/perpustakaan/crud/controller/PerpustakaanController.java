package com.perpustakaan.crud.controller;

import com.perpustakaan.crud.model.Perpustakaan;
import com.perpustakaan.crud.service.PerpustakaanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Perpustakaan")
public class PerpustakaanController {
@Autowired
    private PerpustakaanService perpustakaanService;

    @GetMapping("/all")
    public List<Perpustakaan> getAllPerpustakaan(){
        return perpustakaanService.getAllPerpustakaan();
    }

    @GetMapping("/{No}")
    public Perpustakaan getPerpustakaan(@PathVariable("No") Long No) {
        return perpustakaanService.getPerpustakaan(No);
    }

    @PostMapping
    public Perpustakaan addPerpustakaan(@RequestBody Perpustakaan perpustakaan) {
        return perpustakaanService.addPerpustakaan(perpustakaan);
    }

    @PutMapping("/{No}")
    public Perpustakaan editPerpustakaanById(@PathVariable("No") Long No, @RequestBody Perpustakaan perpustakaan) {
        return perpustakaanService.editPerpustakaan(No, perpustakaan.getNamaPeminjam(), perpustakaan.getNamaBuku(), perpustakaan.getLamaPeminjaman(), perpustakaan.getStatus(), perpustakaan.getKeterangan());
    }

    @DeleteMapping("/{No}")
    public void deletePerpustakaanById(@PathVariable("No") Long No) { perpustakaanService.deletePerpustakaanById(No);}
}
