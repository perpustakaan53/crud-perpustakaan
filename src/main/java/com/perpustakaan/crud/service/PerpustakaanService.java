package com.perpustakaan.crud.service;


import com.perpustakaan.crud.model.Perpustakaan;

import java.util.List;

public interface PerpustakaanService {
      List<Perpustakaan> getAllPerpustakaan();
Perpustakaan addPerpustakaan(Perpustakaan perpustakaan);
Perpustakaan getPerpustakaan(Long No);

Perpustakaan editPerpustakaan(Long id, String namaPeminjam, String namaBuku,String lamaPeminjaman, String status, String keterangan);
void deletePerpustakaanById(Long No);
}
