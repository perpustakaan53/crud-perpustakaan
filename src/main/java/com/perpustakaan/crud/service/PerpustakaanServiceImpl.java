package com.perpustakaan.crud.service;

import com.perpustakaan.crud.model.Perpustakaan;
import com.perpustakaan.crud.repository.PerpustakaanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PerpustakaanServiceImpl implements PerpustakaanService {
    @Autowired
    PerpustakaanRepository perpustakaanRepository;
    @Override
    public List<Perpustakaan> getAllPerpustakaan() {
        return perpustakaanRepository.findAll();
    }

    @Override
    public Perpustakaan addPerpustakaan(Perpustakaan perpustakaan) {
        return perpustakaanRepository.save(perpustakaan);
    }

    @Override
    public Perpustakaan getPerpustakaan(Long No) {
        return perpustakaanRepository.findById(No).get();
    }

    @Override
    public Perpustakaan editPerpustakaan(Long id, String namaPeminjam, String namaBuku, String lamaPeminjaman, String status, String keterangan) {
        Perpustakaan perpustakaan = perpustakaanRepository.findById(id).get();
        perpustakaan.setNamaPeminjam(namaPeminjam);
        perpustakaan.setNamaBuku(namaBuku);
        perpustakaan.setLamaPeminjaman(lamaPeminjaman);
        perpustakaan.setStatus(status);
        perpustakaan.setKeterangan(keterangan);
        return perpustakaanRepository.save(perpustakaan);
    }

    @Override
    public void deletePerpustakaanById(Long No) {
        perpustakaanRepository.deleteById(No);
    }

}
